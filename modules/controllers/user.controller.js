const User = require('../models/user.model.js');
const URL_BASE = process.env.url_base;
const links = `[
  { "rel": "self", "method": "GET", "href": "/users/_id_"},
  { "rel": "create", "method": "POST", "href": "/users/" },
  { "rel": "edit", "method": "PUT", "href": "/users/_id_"},
  { "rel": "delete", "method": "DELETE", "href": "/users/_id_"}
]`;

// Create and Save a new User
exports.create = (req, res) => {
  User.findOne({"email":req.body.email})
  .then(user => {
    User.countDocuments( {}, function(err, result){
      if(err){
          res.status(500).send({
              message: err.message || "Some error occurred while creating the User."
          });
      }
      else{
        if(!user){
          const user = new User(req.body);
          user.id_user=result+1;
          user.logged=true;
          user.save()
          .then(data => {
              data = JSON.parse(JSON.stringify(data));
              data.links =JSON.parse(links.replace(/_id_/gi,data.id_user));
              res.status(201).send(data);
          }).catch(err => {
              res.status(500).send({
                  message: err.message || "Some error occurred while creating the User."
              });
          });
        }else{
          res.status(409).send({
              message: "Email already registered."
          });
        }
      }
    });
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Some error occurred while retrieving users."
      });
  });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    User.find({isActive: true})
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });
};

// Find a single user with a id_user
exports.findOne = (req, res) => {
    User.findOne({"id_user": req.params.id_user,isActive: true})
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id_user
            });
        }
        user = JSON.parse(JSON.stringify(user));
        user.links =JSON.parse(links.replace(/_id_/gi,user.id_user));
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params.id_user
            });
        }
        return res.status(500).send({
            message: err.message || "Error retrieving user with id " + req.params.id_user
        });
    });
};

// Update a user identified by the id_user in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.first_name && !req.body.last_name
          && !req.body.email && !req.body.password) {
        return res.status(400).send({
            message: "User content can not be empty"
        });
    }
    // Find user and update it with the request body
    User.findOneAndUpdate({id_user: req.params.id_user,isActive: true},
      req.body)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id_user
            });
        }
        const ref =JSON.parse(links.replace(/_id_/gi,user.id_user));
        res.status(200).send({message:"User updated successfully!", "id":user.id_user, "links":ref});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params.id_user
            });
        }
        return res.status(500).send({
            message: err.message ||" Error updating user with id " + req.params.id_user
        });
    });
};

// Delete a User with the specified id_user in the request
exports.delete = (req, res) => {
  User.findOneAndUpdate({
    id_user: req.params.id_user,
    isActive: true
  }, {"isActive":false})
  .then(user => {
      if(!user) {
          return res.status(404).send({
              message: "User not found with id " + req.params.id_user
          });
      }
      res.status(200).send({message:'User deleted successfully!', "id":user.id_user});
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Some error occurred while deleting the user."
      });
  });
};

//Login a User with the specified email and password in the request
exports.login =  (req, res) => {
  User.findOneAndUpdate({
    email: req.body.email,
    password: req.body.password
  }, {"logged":true})
  .then(user => {
      if(!user) {
          return res.status(404).send({
              message: "Usuario no válido."
          });
      }
      res.send({message:'Login correcto', user});
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Some error occurred while login a user."
      });
  });
};

//Logout a User with the specified email and password in the request
exports.logout =  (req, res) => {
  User.findOneAndUpdate({
    id_user: req.params.id_user,
    logged: true
  }, {"logged":false})
  .then(user => {
      if(!user) {
          return res.status(404).send({
              message: "Logout failed!"
          });
      }
      res.send({message:'Logout correcto', 'id_user':user.id_user, 'logged':user.logged});
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Some error occurred while logout a user."
      });
  });
};
