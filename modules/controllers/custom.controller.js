const User = require('../models/user.model.js');
const Account = require('../models/account.model.js');

//Custom for divisas
exports.divisas = (req, res) => {
  //en la petición debe llegar los id de las cuentas asi como su saldo y la accion a realizar
  let sourceAmount=req.body.sourceAmount;
  let targetAmount=req.body.targetAmount;
  let amountDebit=req.body.amountDebit;
  let amountAdd=req.body.amountAdd;

  if(sourceAmount-amountDebit>0){
    sourceAmount=sourceAmount-amountDebit;
    targetAmount=targetAmount+amountAdd;
    //actualizamos saldo de cuenta origen
    Account.findOneAndUpdate({id_account: req.body.sourceAccount,
      id_user: req.body.id_user,
      isActive: true},
      {saldo: sourceAmount})
    .then(accountSource => {
        if(!accountSource) {
            return res.status(404).send({
                message: "Account or User not registered"
            });
        }
        //actualizamos saldo de cuenta destino
        /***************************/
        Account.findOneAndUpdate({id_account: req.body.targetAccount,
          id_user: req.body.id_user,
          isActive: true},
          {saldo: targetAmount})
        .then(accountTarget => {
            if(!accountTarget) {
                return res.status(404).send({
                    message: "Account or User not registered"
                });
            }
            res.status(200).send({message:"Account's updated successfully!",
            "accountSource":accountSource.id_account,
            "accountTarget":accountTarget.id_account});
        }).catch(err => {
            if(err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Account or User not registered"
                });
            }
            return res.status(500).send({
                message: err.message ||" Error updating account with id " + req.body.sourceAccount
            });
        });
        /***************************/
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Account or User not registered"
            });
        }
        return res.status(500).send({
            message: err.message ||" Error updating account with id " + req.body.sourceAccount
        });
    });

  }else{
    res.status(404).send({message:'Saldo insuficiente!'});
  }
};
