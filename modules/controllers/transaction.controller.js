const Transaction = require('../models/transaction.model.js');
const Account = require('../models/account.model.js');
const links = `[
  { "rel": "self", "method": "GET", "href": "/accounts/_id_"},
  { "rel": "create", "method": "POST", "href": "/accounts/" }
]`;

// Create and Save a new Transaction
exports.create = (req, res) => {
  Account.findOne({id_account: req.params.id_account, id_user: req.body.id_user})
  .then(account => {
      if(!account) {
          return res.status(404).send({
              message: "Account or User not registered"
          });
      }else{
        if(req.body.account_target){
          Account.findOne({cta: req.body.account_target})
          .then(account_target => {
              if(!(account_target!= null && account.divisa.toUpperCase()==account_target.divisa.toUpperCase()
                  && account.divisa.toUpperCase()==req.body.divisa.toUpperCase())) {
                  return res.status(404).send({
                      message: "Account not registered or the divisas are different"
                  });
              }else{
                account_target.saldo=account_target.saldo+req.body.amount;
                account_target.save();
                account.saldo=account.saldo-req.body.amount;
                account.save();
                Transaction.countDocuments( {}, function(err, result){
                  if(err){
                      res.status(500).send({
                          message: err.message || "Some error occurred while creating the Transaction."
                      });
                  }
                  else{
                      const transaction = new Transaction(req.body);
                      transaction.id_transaction=result+1;
                      transaction.id_account=req.params.id_account;
                      transaction.save()
                      .then(data => {
                        const transaction_target = new Transaction(req.body);
                          transaction_target.id_transaction=result+2;
                          transaction_target.type="ABONO";
                          transaction_target.id_account=account_target.id_account;
                          transaction_target.id_user=account_target.id_user;
                          transaction_target.save()
                          data = JSON.parse(JSON.stringify(data));
                          data.id_user_transfered=account_target.id_user;
                          data.transfered=true;
                          data.links =JSON.parse(links.replace(/_id_/gi,data.id_transaction));
                          res.status(201).send(data);
                      }).catch(err => {
                          res.status(500).send({
                              message: err.message || "Some error occurred while creating the Account."
                          });
                      });
                  }
                });
              }
          }).catch(err => {
              if(err.kind === 'ObjectId') {
                  return res.status(404).send({
                      message: "Account or User not registered"
                  });
              }
              return res.status(500).send({
                  message: err.message || "Error retrieving account with id " + req.params.id_account
              });
          });
        }else{
          return res.status(404).send({
              message: "Target account not indicated or the divisas are different"
          });
        }
      }
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          return res.status(404).send({
              message: "Account or User not registered"
          });
      }
      return res.status(500).send({
          message: err.message || "Error retrieving account with id " + req.params.id_account
      });
  });
};

// Retrieve and return all transactions from the database.
exports.findAll = (req, res) => {
    Transaction.find({"id_account":req.params.id_account})
    .then(transactions => {
        if(!transactions[0]) {
            return res.status(404).send({
                message: "Account not registered or not transactions registered"
            });
        }
        res.send(transactions);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving transactions."
        });
    });
};

// Find a single transaction with a id_transaction
exports.findOne = (req, res) => {
    Transaction.findOne({"id_account": req.body.id_account, "id_transaction":req.body.id_transaction})
    .then(transaction => {
        if(!transaction) {

            return res.status(404).send({
                message: "Account not registered or not transactions registered"
            });
        }
        transaction = JSON.parse(JSON.stringify(transaction));
        transaction.links =JSON.parse(links.replace(/_id_/gi,transaction.id_transaction));
        res.send(transaction);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Account not registered or not transactions registered"
            });
        }
        return res.status(500).send({
            message: err.message || "Error retrieving transaction with id " + req.body.id_transaction
        });
    });
};
