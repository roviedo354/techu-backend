const User = require('../models/user.model.js');
const Account = require('../models/account.model.js');
const cta = process.env.pref_cta;
const cci = process.env.pref_cci;
const links = `[
  { "rel": "self", "method": "GET", "href": "/accounts/_id_"},
  { "rel": "create", "method": "POST", "href": "/accounts/" },
  { "rel": "edit", "method": "PUT", "href": "/accounts/_id_"},
  { "rel": "delete", "method": "DELETE", "href": "/accounts/_id_"}
]`;

// Create and Save a new Account
exports.create = (req, res) => {
  User.findOne({id_user: req.params.id_user,isActive: true})
  .then(user => {
      if(!user) {
          return res.status(404).send({
              message: "User not found with id " + req.params.id_user
          });
      }
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          return res.status(404).send({
              message: "User not found with id " + req.params.id_user
          });
      }
      return res.status(500).send({
          message: "Error retrieving user with id " + req.params.id_user
      });
  });

  /*Account.findOne({ $or: [{ account: req.body.account }, { cci: req.body.cci }] })
  .then(account => {*/
    Account.countDocuments( {}, function(err, result){
      if(err){
          res.status(500).send({
              message: err.message || "Some error occurred while creating the Account."
          });
      }
      else{
        //if(!account){
          const account = new Account(req.body);
          account.id_account=result+1;
          account.id_user=req.params.id_user;
          account.cta=cta.replace(/ABCDEF/gi,zfill(result+1, 6));
          account.cci=cci.replace(/ABCDEF/gi,zfill(result+1, 6));
          account.save()
          .then(data => {
              data = JSON.parse(JSON.stringify(data));
              data.links =JSON.parse(links.replace(/_id_/gi,account.id_account));
              res.status(201).send(data);
          }).catch(err => {
              res.status(500).send({
                  message: err.message || "Some error occurred while creating the Account."
              });
          });
        /*}else{
          res.status(409).send({
              message: "Account or CCI already registered."
          });
        }*/
      }
    });
  /*}).catch(err => {
      res.status(500).send({
          message: err.message || "Some error occurred while retrieving notes."
      });
  });*/
};

// Retrieve and return all accounts from the database.
exports.findAll = (req, res) => {
    Account.find({"id_user":req.params.id_user,isActive: true})
    .then(accounts => {
        if(!accounts[0]) {
            return res.status(404).send({
                message: "Accounts or User not registered "
            });
        }
        res.send(accounts);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving accounts."
        });
    });
};

// Find a single account with a id_account
exports.findOne = (req, res) => {
    Account.findOne({"id_account": req.query.id_account, "id_user":req.query.id_user, isActive: true})
    .then(account => {
        if(!account) {
            return res.status(404).send({
                message: "Account not found with id " + req.query.id_account
            });
        }
        account = JSON.parse(JSON.stringify(account));
        user.links =JSON.parse(links.replace(/_id_/gi,account.id_account));
        res.send(account);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Account not found with id " + req.query.id_account
            });
        }
        return res.status(500).send({
            message: "Error retrieving account with id " + req.query.id_account
        });
    });
};

// Update a account identified by the id_account in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.saldo) {
        return res.status(400).send({
            message: "Saldo can not be empty"
        });
    }

    // Find account and update it with the request body
    Account.findOneAndUpdate({id_account: req.query.id_account,
      id_user: req.query.id_user,
      isActive: true},
      req.body)
    .then(account => {
        if(!account) {
            return res.status(404).send({
                message: "Account or User not registered"
            });
        }
        const ref =JSON.parse(links.replace(/_id_/gi,account.id_account));
        res.status(200).send({message:"Account updated successfully!", "id":account.id_account,
        "links":ref});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Account or User not registered"
            });
        }
        return res.status(500).send({
            message: err.message ||" Error updating account with id " + req.query.id_account
        });
    });
};

// Delete a Account with the specified id_account in the request
exports.delete = (req, res) => {
  Account.findOneAndUpdate({
    id_account: req.query.id_account,
    id_user: req.query.id_user,
    isActive: true
  }, {"isActive":false})
  .then(account => {
      if(!account) {
          return res.status(404).send({
              message: "Account or User not registered"
          });
      }
      res.status(200).send({message:'Account deleted successfully!', "id":account.id_account});
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Some error occurred while deleting the account."
      });
  });
};

function zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (width <= length) {
        if (number < 0) {
             return ("-" + numberOutput.toString());
        } else {
             return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
};
