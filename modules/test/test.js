var request = require('supertest');
var app = require('../../server.js');
describe('GET /', function() {
 it('respond with TechU-Proyecto Final', function(done) {
 //navigate to root and check the the response is "TechU-Proyecto Final"
 request(app).get('/').expect('TechU-Proyecto Final', done);
 });
});
