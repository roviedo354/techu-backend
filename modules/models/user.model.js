const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  id_user: {
    type: Number
  },
  first_name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  last_name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  logged: {
    type: Boolean
  },
  isActive: {
    type: Boolean,
    default: true
  }
});

const User = mongoose.model("User", UserSchema);
module.exports = User;
