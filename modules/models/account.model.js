const mongoose = require('mongoose');

const AccountSchema = new mongoose.Schema({
  id_account: {
    type: Number
  },
  description: {
    type: String,
    required: true,
    trim: true,
    uppercase: true
  },
  cta: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  cci: {
    type: String,
    required: true,
    trim: true,
    lowercase: true
  },
  divisa: {
    type: String,
    required: true,
    trim: true,
    uppercase: true
  },
  saldo: {
    type: Number,
    default: 0
  },
  id_user: {
    type: Number,
    required: true
  },
  isActive: {
    type: Boolean,
    default: true
  }
});

const Account = mongoose.model("Account", AccountSchema);
module.exports = Account;
