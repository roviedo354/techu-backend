const mongoose = require('mongoose');

const TransactionSchema = new mongoose.Schema({
  id_transaction: {
    type: Number
  },
  description: {
    type: String,
    required: true,
    trim: true,
    uppercase: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  divisa: {
    type: String,
    required: true,
    trim: true,
    uppercase: true
  },
  type: {
    type: String,
    required: true,
    trim: true,
    uppercase: true
  },
  id_account: {
    type: Number,
    required: true
  },
  id_account_2: {
    type: Number
  },
  amount: {
    type: Number,
    required: true
  }
});

const Transaction = mongoose.model("Transaction", TransactionSchema);
module.exports = Transaction;
