require('dotenv').config();
module.exports = {
    url: `mongodb://${process.env.uri}/${process.env.db}`
}

/*module.exports = {
    url: `mongodb://${process.env.user}:${process.env.pass}@${process.env.uri}/${process.env.db}`
}*/
