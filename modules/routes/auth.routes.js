const URL_BASE = process.env.url_base;
module.exports = (app) => {
    const user = require('../controllers/user.controller.js');

    // Login user
    app.post(URL_BASE+'/login', user.login);

    // Logout user
    app.post(URL_BASE+'/logout/:id_user', user.logout);
}
