const URL_BASE = process.env.url_base;
module.exports = (app) => {
    const custom = require('../controllers/custom.controller.js');

    // Custom for divisas
    app.post(URL_BASE+'/divisas', custom.divisas);

}
