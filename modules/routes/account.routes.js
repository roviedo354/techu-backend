const URL_BASE = process.env.url_base;
module.exports = (app) => {
    const account = require('../controllers/account.controller.js');

    // Create a new Account
    app.post(URL_BASE+'/accounts/:id_user', account.create);

    // Retrieve all Accounts
    app.get(URL_BASE+'/accounts/:id_user', account.findAll);

    // Retrieve a single Account with id_account
    app.get(URL_BASE+'/accounts', account.findOne);
    //app.get(URL_BASE+'/accounts/:id_user/:id_account', account.findOne);

    // Update a Note with id_account
    app.put(URL_BASE+'/accounts', account.update);
    //app.put(URL_BASE+'/accounts/:id_user/:id_account', account.update);

    // Delete a Note with id_account
    app.delete(URL_BASE+'/accounts', account.delete);
    //app.delete(URL_BASE+'/accounts/:id_user/:id_account', account.delete);
}
