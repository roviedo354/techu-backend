const URL_BASE = process.env.url_base;
module.exports = (app) => {
    const transaction = require('../controllers/transaction.controller.js');

    // Create a new Transaction
    app.post(URL_BASE+'/transactions/:id_account', transaction.create);
    //app.post(URL_BASE+'/transactions/:id_user/:id_account', transaction.create);

    // Retrieve all Transactions
    app.get(URL_BASE+'/transactions/:id_account', transaction.findAll);

    // Retrieve a single Transaction with id_transaction
    app.get(URL_BASE+'/transactions', transaction.findOne);
    //app.get(URL_BASE+'/transactions/:id_account/:id_transaction', transaction.findOne);
}
