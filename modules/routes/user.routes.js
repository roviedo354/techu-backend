const URL_BASE = process.env.url_base;
module.exports = (app) => {
    const user = require('../controllers/user.controller.js');

    // Create a new User
    app.post(URL_BASE+'/users', user.create);

    // Retrieve all Users
    app.get(URL_BASE+'/users', user.findAll);

    // Retrieve a single User with id_user
    app.get(URL_BASE+'/users/:id_user', user.findOne);

    // Update a User with id_user
    app.put(URL_BASE+'/users/:id_user', user.update);

    // Delete a User with id_user
    app.delete(URL_BASE+'/users/:id_user', user.delete);
}
