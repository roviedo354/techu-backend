# API REST techu-backend

Este API REST esta diseñado como parte del proyecto final de la 2da edición del programa TechU Practitioner 2020.

Este API REST está desplegado en un servicio en cloud de Digital Ocean : [API REST Digital Ocean] (http://68.183.98.140:3000)

El código esta alojado en Bitbucket, en el siguiente repositorio: [API REST techu-backend] (https://bitbucket.org/roviedo354/techu-backend/src/master/)

## Introducción
Este API esta desarrollado para una supuesta Banca por Internet, en la cual se podrá dar de alta a usuarios, cuentas y transacciones, así como la implementación de la funcionalidad de cambio de divisas.
Se ha contemplado distintos casos de uso, los cuales serán descritos líneas más abajo, seguidos de la ruta correspondiente de los links para su invocación.

## Casos de Uso
### GET:
- **Users**

	- **api/users** -> Devuelve todos los usuarios activos.
	- **api/users/:id_user** -> Devuelve el usuario con el ID indicado.

- **Accounts**

  - **api/accounts/:id_user** -> Devuelve todas los cuentas activas, correspondientes al cliente con el ID indicado.
  - **api/accounts** -> Devuelve una cuenta activa, con el ID de usuario e ID de cuenta correspondientes, indicadas por query string.

- **Transactions**

  - **api/transactions/:id_account** -> Devuelve todas las transacciones correspondientes a la cuenta con el ID indicado.
  - **api/transactions** -> Devuelve una transacción, con el ID de cuenta e ID de transacción correspondientes, indicadas por query string.

### POST
- **Users**

	- **api/users** -> Crea un nuevo usuario en la base de datos, a partir de un JSON indicado en el cuerpo de la petición.

- **Auth**

  - **api/login/** -> Realiza el login del usuario, siendo necesario indicar en el cuerpo de la petición el email y password del usuario a loguear.
  - **api/logout/** -> Realiza el logout del usuario, siendo necesario indicar en la petición el id del usuario a hacer logout.

- **Accounts**

	- **api/accounts/:id_user** -> Crea una nueva cuenta en la base de datos, a partir de un JSON indicado en el cuerpo de la petición, para el usuario con ID indicado en la petición.

- **Transactions**

	- **api/transactions/:id_account** -> Crea una nueva transacción en la base de datos, a partir de un JSON indicado en el cuerpo de la petición, para la cuenta con ID indicada en la petición.

- **Customs**

	- **api/divisas** -> funcionalidad custom que realiza el cambio de divisas.


### DELETE
- **Users**

	- **api/users/:id_user** -> Realiza la elminación lógica del usuario con el ID correspondiente, indicado en la petición.

- **Accounts**

	- **api/accounts** -> Realiza la elminación lógica de una cuenta, con el ID de usuario e ID de cuenta correspondientes, indicadas por query string.

- **Transactions**

	- No se ha contemplado la eliminación de transacciones, puesto que la información contenida es de suma importancia, no siendo una buena practica eliminarla.


### PUT
- **Users**

	- **api/users/:id_user** -> Realiza la modificación del usuario, de los campos indicados en el cuerpo de la petición, con el ID correspondiente indicado en la petición.

- **Accounts**

  - **api/accounts** -> Realiza la modificación de la cuenta, de los campos indicados en el cuerpo de la petición, con el ID de usuario e ID de cuenta correspondientes, indicadas por query string.

- **Transactions**

  - No se ha contemplado la modificación de transacciones debido a que se debe conservar la integridad de la información.

## Lista de comandos para puesta en marcha
#### Para poner en marcha la aplicación tendremos que realizar los siguientes comandos:

 `npm install 		 //con esto instalaremos los modulos necesarios para node.js`


Luego de ejecutado el comando anterior será necesaria la construcción del contenedor docker-compose, para esto se ejecuta el comando:

`docker-compose build`

> Este comando construirá los servicios indicados en el archivo docker-compose.yml, en nuestro caso construirá el servicio correspondiente al backend, para el caso de la BD al usar una imagen de Mongo no se realizará ninguna construcción adicional.

Una vez realizada la construcción procederemos a levantar el contenedor, esto se realiza con la instrucción:

`docker-compose up -d`

> Este comando levantará el contenedor con los servicios que fueron construidos por el comando anterior, en el caso de la BD se realizará previamente un pulling de la imagen de MongoDB.

Para realizar cambios sin necesidad de reiniciar o volver construir, lo que se debe realizar es en el archivo Dockerfile comentar el comando CMD ["npm","run","prod"], y descomentar el comando:

`CMD ["npm","run","dev"]`

> Esto es posible dado que en la configuración de nuestro docker-compose.yml para nuestros servicios se maneja la etiqueta volumes, lo cual nos permite actualizar los cambios tanto de nuestro directorio de trabajo como del contenedor.

##### Para realizar las pruebas usaremos una aplicacion llamada PostMan, esta aplicacion permite realizar las peticiones necesarias para un API REST (GET, POST, DELETE, PUT).

##### Para el uso de lo servicios a través de un front se ha creado el proyecto https://bitbucket.org/roviedo354/techu-frontend/admin, el cual implementa el consumo de los servicios creados.

## Requisitos "Adicionales" implementados
Para la implementación de esta API, se ha usado, además de las tecnologías aprendidas durante el curso, las siguientes:

- Routes, para la separación en módulos.

- Docker-Compose, para el trabajo de diferentes servicios en un mismo contenedor, en nuestro proyecto usamos un servicio donde está alejado nuestro backend y en otro servicio alojamos nuestra BD en mongo.

- Mongoose, esto para el trabajo con nuestra BD en mongo, Mongoose nos provee de métodos y funcionalidades para un mejor trabajo con Mongo.

- Jenkins, esto para realizar un trabajo de CI/CD, en nuestro proyecto se encarga de realizar la construcción de la imagen de docker, subirla a Docker Hub, y además realizar el despliegue a Digital Ocean.

- Digital Ocean, para el despliegue de nuestra API en algun servicio en la nube, nuestra API se encuentra alojada en [API REST Digital Ocean] (http://68.183.98.140:3000)
