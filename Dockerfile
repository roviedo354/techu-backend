# Imagen docker base inicial
FROM node:latest

# Crear directorio de trabajo del contenedor Docker
WORKDIR /docker-api-techu

# Copiar archivos del proyecto en el directorio de trabajo de Docker
ADD . /docker-api-techu

# Instalar dependencias producción del proyecto
# RUN npm install --only=production

# Puerto donde exponemos contenedor
EXPOSE 3000

# Comando para lanzar la app
CMD ["npm","run","prod"]
#CMD ["npm","run","dev"]
