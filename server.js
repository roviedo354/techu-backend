const express = require('express');
const body_parser=require('body-parser');
const URL_BASE = '/techu/v2';
const mongoose = require('mongoose');
const app = express();
const port=process.env.port || 3000;
const cors=require('cors');

app.use(express.json());
app.use(body_parser.json());

app.use(cors());
app.options('*', cors());

// Configuring the database
const dbConfig = require('./modules/config/database.js');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url, {
	useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify:false
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

require('./modules/routes/user.routes.js')(app);
require('./modules/routes/auth.routes.js')(app);
require('./modules/routes/account.routes.js')(app);
require('./modules/routes/transaction.routes.js')(app);
require('./modules/routes/custom.routes.js')(app);

app.get('/', (req, res) => {
  res.send('TechU-Proyecto Final')
})

app.listen(port, () => console.log('API ready port: '+port+'...'));

module.exports = app;
